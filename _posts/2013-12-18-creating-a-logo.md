---
layout: post
title: "Creating a Logo"
---

I though it was about time to add a logo to the site. I'm not a graphic designer
so I just imitate what I can.

The basics of a logo is that it has to have a unique twist that sets it apart
and it also has to be usable. This means recognizable at different sizes and in
different contexts.

The theme is a mix of traditional publishing (coming from LaTeX) and modern web
(this service). For this reason I chose an extra bold italic serif L from the
[Simonetta][simonetta] font (standing for "LaTeX") and a preceding extra bold
sans serif M from the [Changa One][changa-one] font (standing for "Multi"). The
M is superscripted as a homage to the way TeX and LaTeX are written. (You'll
have noticed that I never write Multilatex as MultiLaTeX because I believe it's
silly to mix up names and logos.) Someone could look at it and just see a single
long looping curve, but there is a bit of reasoning behind it. To make it
more visible I also created a rectangle one.

![Multilatex logo](/s/blog/img/logo.png)

I created the SVG with Inkscape and I used [Fontello][fontello] to convert it to
fonts usable in browsers.

The whole favicon situation is horrible, so I used [Iconifier][iconifier] to
generate all the favicon sizes.

I won't go into styling the website any more now, so I haven't picked a color
theme. The logo will be just white or black.

[simonetta]: http://www.google.com/fonts/specimen/Simonetta
[changa-one]: http://www.google.com/fonts/specimen/Changa+One
[fontello]: http://fontello.com/
[iconifier]: http://iconifier.net/
